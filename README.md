# transaction_match

***ファイル構成***  
```
transaction_match - Match.py  
				  L InputTransaction.txt  
				  L InputInvoice.txt  
```
***ファイル詳細***  
・Match.py
```
Search関数  
``` 

・InputTransaction.txt  
```
データセット  
左から順に"商品名","小売店名","金額","購入個数"  
```

・InputInvoice.txt
```  
データセット  
左から順に"商品名","小売店名","金額","購入個数","一個あたりの値引き金額"  
```

***実行環境***  
```
Python 3.7.3  
```
  
***実行方法***  
```
python3 Match.py  
```